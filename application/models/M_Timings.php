<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Ramsey\Uuid\Uuid;

class M_Timings extends DH_Model {

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
    }

    public function Create($id, $timestamp)
    {
        $data = [
            'id' => $id,
            'timestamp' => $timestamp
        ];
        $this->db->insert('timings', $data);
    }

    



}

/* End of file M_Timings.php */
/* Location: ./application/models/M_Timings.php */