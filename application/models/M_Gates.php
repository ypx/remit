<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_Gates extends DH_Model {

    public function Get($gate_id = null)
    {
        if($gate_id == null) {
            $this->db->order_by('created_at', 'desc');
            return $this->db->get('gates')->result();            
        } else {
            return $this->db->get_where('gates', ['id' => $gate_id])->row();            
        }
    }

    public function GetActive()
    {
        $this->db->where('status', 1);
        return $this->db->get('gates')->result();
    }

    public function CreatePlayers($data)
    {
        $this->db->insert('gates_players', $data);
    }

    public function GetPlayers($gate_id)
    {
        $this->db->where('gate_id', $gate_id);
        $this->db->order_by('created_at', 'asc');
        return $this->db->get('gates_players')->result();
    }

    public function GetLastTiming($gate_id)
    {
        $sql = "
            SELECT timestamp
            FROM 
                gates_timings gt, timings t
            WHERE 
                t.id = gt.timing_id and 
                gt.gate_id = '{$gate_id}'
            ORDER BY timestamp desc
        ";
        $query = $this->db->query($sql);
        return $query->row();
    }

    public function GetTypeStart()
    {
        $sql = "
            SELECT g.id, g.name, timestamp
            FROM 
                gates g, gates_timings gt, timings t
            WHERE 
                t.id = gt.timing_id and
                g.id = gt.gate_id and
                g.type = 0
            ORDER BY timestamp desc
        ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function GetAllTiming($gate_id)
    {
        $sql = "
            SELECT g.name, timestamp
            FROM 
                gates g, gates_timings gt, timings t
            WHERE 
                t.id = gt.timing_id and 
                g.id = gt.gate_id and
                gt.gate_id = '{$gate_id}'
            ORDER BY timestamp asc
        ";
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function CreateTimings($id, $gate_id)
    {
        $data = [
            'timing_id' => $id,
            'gate_id' => $gate_id
        ];
        $this->db->set('gate_id', $gate_id);
        $this->db->insert('gates_timings', $data);
    }



    public function InsupTimings($id, $gate_id)
    {
        $this->db->where('gate_id', $gate_id);
        $q = $this->db->get('gates_timings');

        $data = [
            'timing_id' => $id,
            'gate_id' => $gate_id
        ];

        if($q->num_rows() > 0 ) 
        {
            $this->db->where('gate_id', $gate_id);
            $this->db->update('gates_timings', $data);
        } else {
            $this->db->set('gate_id', $gate_id);
            $this->db->insert('gates_timings', $data);
        }

        // $data = [
        //     'timing_id' => $id,
        //     'gate_id' => $gate_id
        // ];
        // $this->db->set($data);
        // $this->db->where('gate_id', $gate_id);
        // $this->db->insert('gates_timings');
    }

}

/* End of file M_Gates.php */
/* Location: ./application/models/M_Gates.php */