<?php $this->load->view('partials/head'); ?>

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2">Tambah Gate Baru</h1>
  <div class="btn-toolbar mb-2 mb-md-0">
    <?php if($action == 'update'): ?>
    <div class="btn-group mr-2">
      <a href="<?php echo base_url('gates/action_delete/' . $gate->id) ?>" class="btn btn-sm btn-outline-danger">Hapus Gate Ini</a>
      <?php if(@$gate->status == '0'): ?>
      <a href="<?php echo base_url('gates/action_update_status/' . $gate->id . '/1') ?>" class="btn btn-sm btn-outline-success">Jadikan Aktif</a>
      <?php elseif(@$gate->status == '1'): ?>
      <a href="<?php echo base_url('gates/action_update_status/' . $gate->id . '/0') ?>" class="btn btn-sm btn-outline-danger">Jadikan Non aktif</a>
      <?php endif; ?>
    </div>
    <?php endif; ?>
    <a href="<?php echo base_url('gates') ?>" class="btn btn-sm btn-secondary">
      Kembali
    </a>
  </div>
</div>

<form method="POST" action="<?php echo base_url('gates/action_' . $action) ?>">
  
  <?php if($action == 'update'): ?>
    <input type="hidden" name="id" value="<?php echo @$gate->id ?>">
  <?php endif; ?>

  <div class="form-group">
    <label>Nama Gate</label>
    <input type="text" class="form-control" name="name" value="<?php echo @$gate->name ?>" required>
  </div>

  <div class="form-group">
    <label>Deskripsi Gate</label>
    <input type="text" class="form-control" name="description" value="<?php echo @$gate->description ?>">
  </div>
  
  <?php if($action == 'create'): ?>
  <div class="form-group">
    <label>Tipe Gate</label>
    <select name="type" id="" class="form-control" required>
      <option value="">-- Pilih Type Gate --</option>
      <option value="0" <?php if(@$gate->type == '0') echo 'selected' ?>>Start</option>
      <option value="1" <?php if(@$gate->type == '1') echo 'selected' ?>>Finish</option>
    </select>
  </div>
  <?php endif; ?>

  <button type="submit" class="btn btn-primary">Simpan</button>
  
</form>

<?php $this->load->view('partials/foot'); ?>
