<?php $this->load->view('partials/head'); ?>

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2">Result Timing Gate : <?php echo $gate->name ?></h1>
  <div class="btn-toolbar mb-2 mb-md-0">
    <a href="<?php echo base_url('gates') ?>" class="btn btn-sm btn-primary">
      Kembali
    </a>
  </div>
</div>

<form>
  <div class="form-group">
    <label for="">Gate Start</label>
    <select id="gateStartChooser" class="form-control">
      <option value="">-- Pilih Gate Start --</option>
      <?php foreach($gates as $gt): ?>
        <option <?php if(@$gate_from == $gt->id) echo 'selected' ?> data-timestamp="<?php echo $gt->timestamp ?>" data-from="<?php echo $gate->id ?>" value="<?php echo $gt->id ?>">
          <?php echo $gt->name ?> - 
          Start Pukul: 
          <?php 
          if($gt->timestamp) {
            $seconds = $gt->timestamp / 1000;
            $micro = substr($gt->timestamp, -3);
            echo date("H:i:s." . $micro, $seconds);
          } else {
            echo 'Belum Tercatat';
          }
          ?>
        </option>
      <?php endforeach; ?>
    </select>
  </div>
</form>

<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Peserta</th>
      <th scope="col">Timestamp</th>
      <th scope="col">Elapsed Time (based on gate start choose)</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($timings as $i => $time): ?>
    <tr>
      <td><?php echo $i + 1 ?></td>
      <td><?php echo @$players[$i]->player_id ?></td>
      <td>
        <?php 
          $seconds = $time->timestamp / 1000;
          $micro = substr($time->timestamp, -3);
          echo date("H:i:s." . $micro, $seconds);
        ?>
      </td>
      <td>
        <?php if(@$gate_from_timestamp): ?>
          <?php echo microtime_diff($gate_from_timestamp->timestamp, $time->timestamp) ?>
        <?php endif; ?>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<?php $this->load->view('partials/foot'); ?>