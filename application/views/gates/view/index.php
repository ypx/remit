<?php $this->load->view('partials/head'); ?>

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2">Manajemen Gate</h1>
  <div class="btn-toolbar mb-2 mb-md-0">
    <!-- <div class="btn-group mr-2">
      <button class="btn btn-sm btn-outline-secondary">Share</button>
      <button class="btn btn-sm btn-outline-secondary">Export</button>
    </div> -->
    <a href="<?php echo base_url('gates/form') ?>" class="btn btn-sm btn-primary">
      Tambah Gates
    </a>
  </div>
</div>

<table class="table table-bordered table-striped">
  <thead>
    <tr>
      <th scope="col">Nama</th>
      <th scope="col">Deskripsi</th>
      <th scope="col">Type</th>
      <th scope="col">Status</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($gates as $gate): ?>
    <tr>
      <td><?php echo $gate->name ?></td>
      <td><?php echo $gate->description ?></td>
      <td><?php echo ($gate->type == '1') ? 'Finish' : 'Start' ?></td>
      <td>
        <?php if($gate->status == '1'): ?>
          <span class="badge badge-pill badge-success">Aktif</span>
        <?php else: ?>
          <span class="badge badge-pill badge-danger">Non Aktif</span>
        <?php endif; ?>
      </td>
      <td>
        <a href="<?php echo base_url('gates/result/' . $gate->id) ?>" class="btn btn-sm btn-outline-success">Result</a>
        <a href="<?php echo base_url('gates/form/' . $gate->id) ?>" class="btn btn-sm btn-outline-primary">Edit</a>
        <a href="<?php echo base_url('gates/action_delete/' . $gate->id) ?>" class="btn btn-sm btn-outline-danger">Hapus</a>
        <?php if($gate->status == '1'): ?>
          <a href="<?php echo base_url('gates/action_update_status/' . $gate->id . '/0') ?>" class="btn btn-sm btn-outline-danger">Non-aktifkan</a>
        <?php else: ?>
          <a href="<?php echo base_url('gates/action_update_status/' . $gate->id . '/1') ?>" class="btn btn-sm btn-outline-success">Aktifkan</a>
        <?php endif; ?>
      </td>
    </tr>
    <?php endforeach; ?>
  </tbody>
</table>

<?php $this->load->view('partials/foot'); ?>