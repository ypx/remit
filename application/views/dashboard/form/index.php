<?php $this->load->view('partials/head'); ?>

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h1 class="h2">Timer</h1>
</div>

<form>
  <div class="form-group">
    <select id="gateChooser" class="form-control">
      <option value="">-- Pilih Gates --</option>
      <?php foreach($gates as $gate): ?>
        <option <?php if($gate->id == @$gate_id) echo 'selected' ?> value="<?php echo $gate->id ?>" data-type="<?php echo $gate->type ?>"><?php echo $gate->name ?> (<?php echo ($gate->type == 1) ? 'FINISH' : 'START' ?>)</option>
      <?php endforeach; ?>
    </select>
  </div>
</form>

<?php if(@$action == 'start'): ?>
  <div class="alert alert-warning" id="timestamp">
    Waktu start terakhir tercatat: <strong><?php echo $timestamp ?></strong>
  </div>
  <a id="timeTrigger" href="#" data-type="0" data-id="<?php echo $gate_id ?>" class="btn btn-block btn-lg btn-success">Mulai</a>
<?php endif; ?>

<?php if(@$action == 'finish'): ?>
  <a href="<?php echo base_url('player/' . $gate_id) ?>" class="btn btn-block btn-lg btn-primary">Player</a>
  <a href="<?php echo base_url('timer/' . $gate_id) ?>" class="btn btn-block btn-lg btn-warning">Timer</a>
<?php endif; ?>

<?php if(@$action == 'timer'): ?>
  <div class="alert alert-warning" id="timestamp">
    Waktu finish terakhir tercatat: <strong><?php echo $timestamp ?></strong>
  </div>
  <a id="timeTrigger" href="#" data-type="1" data-id="<?php echo $gate_id ?>" class="btn btn-block btn-lg btn-warning">Catat</a>
<?php endif; ?>

<?php if(@$action == 'player'): ?>
  <form id="playerForm">
    <div class="form-group">
      <input type="text" class="form-control" name="player_id" placeholder="Input Nomor Punggung">
      <input type="hidden" name="gate_id" value="<?php echo $gate_id ?>">
    </div>
    <button type="submit" class="btn btn-block btn-lg btn-primary">Catat</button>
  </form>
  <hr>
  <table class="table table-bordered table-striped">
    <thead>
      <tr>
        <th>No</th>
        <th>No Peserta</th>
        <th>Aksi</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach($players as $i => $player): ?>
      <tr>
        <td><?php echo $i+1 ?></td>
        <td><?php echo $player->player_id ?></td>
        <td>
          <a href="<?php echo base_url('gates/action_player_delete/' . $player->player_id . '/' . $gate_id) ?>" class="btn btn-sm btn-outline-danger">Hapus</a>
        </td>
      </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
<?php endif; ?>


<?php $this->load->view('partials/foot'); ?>