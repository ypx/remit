<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'dashboard/form';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['dashboard'] = 'dashboard/form';
$route['gates'] = 'gates/view';
$route['start/(:any)'] = 'timer/start/$1';
$route['finish/(:any)'] = 'timer/finish/$1';
$route['player/(:any)'] = 'timer/player/$1';
$route['timer/action_timer'] = 'timer/action_timer';
$route['timer/action_player'] = 'timer/action_player';
$route['timer/(:any)'] = 'timer/timer/$1';