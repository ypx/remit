<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Ramsey\Uuid\Uuid;

class Timer extends DH_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Gates', 'Gates');
        $this->load->model('M_Timings', 'Timings');
    }

    public function start($gate_id)
    {
        $data['gates'] = $this->Gates->GetActive();
        
        $timestamp = $this->Gates->GetLastTiming($gate_id);
        if($timestamp) {
            $seconds = $timestamp->timestamp / 1000;
            $micro = substr($timestamp->timestamp, -3);
            $data['timestamp'] = date("H:i:s." . $micro, $seconds);
        } else {
            $data['timestamp'] = 'Belum Tercatat';
        }

        $data['gate_id'] = $gate_id;
        $data['action'] = 'start';
        $this->load->view('dashboard/form/index', $data, FALSE);
    }

    public function finish($gate_id)
    {
        $data['gates'] = $this->Gates->GetActive();
        $data['gate_id'] = $gate_id;
        $data['action'] = 'finish';
        $this->load->view('dashboard/form/index', $data, FALSE);
        # code...
    }

    public function timer($gate_id)
    {
        $data['gates'] = $this->Gates->GetActive();

        $timestamp = $this->Gates->GetLastTiming($gate_id);
        if($timestamp) {
            $seconds = $timestamp->timestamp / 1000;
            $micro = substr($timestamp->timestamp, -3);
            $data['timestamp'] = date("H:i:s." . $micro, $seconds);
        } else {
            $data['timestamp'] = 'Belum Tercatat';
        }

        $data['gate_id'] = $gate_id;
        $data['action'] = 'timer';
        $this->load->view('dashboard/form/index', $data, FALSE);
        # code...
    }

    public function player($gate_id)
    {

        $data['gates'] = $this->Gates->GetActive();
        $data['players'] = $this->Gates->GetPlayers($gate_id);
        $data['gate_id'] = $gate_id;
        $data['action'] = 'player';
        $this->load->view('dashboard/form/index', $data, FALSE);
        # code...
    }

    public function action_timer()
    {
        $id = Uuid::uuid1();
        $this->Timings->Create($id, $this->input->post('timestamp'));
        if($this->input->post('type') == 0) {
            $this->Gates->InsupTimings($id, $this->input->post('gate_id'));
        } else {
            $this->Gates->CreateTimings($id, $this->input->post('gate_id'));
        }

        $timestamp = $this->input->post('timestamp');
        $seconds = $timestamp / 1000;
        $micro = substr($timestamp, -3);
        echo date("H:i:s." . $micro, $seconds);
    }

    public function action_player()
    {
        $data = [
            'player_id' => $this->input->post('player_id'),
            'gate_id' => $this->input->post('gate_id'),
            'created_at' => round(microtime(true) * 1000)
        ];
        $this->Gates->CreatePlayers($data);
        echo $this->input->post('gate_id');
    }

}

/* End of file Timer.php */
/* Location: ./application/controllers/Timer.php */