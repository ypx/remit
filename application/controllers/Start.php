<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Start extends DH_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Gates', 'Gates');
    }

    public function index()
    {
        
    }

    public function form($gate_id)
    {
        $data['gates'] = $this->Gates->GetActive();
        $data['gate_id'] = $gate_id;
        $data['action'] = 'start';
        $this->load->view('dashboard/form/index', $data, FALSE);
        # code...
    }

}

/* End of file Start.php */
/* Location: ./application/controllers/Start.php */