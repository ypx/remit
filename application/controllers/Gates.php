<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Ramsey\Uuid\Uuid;

class Gates extends DH_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Gates', 'Gates');
        //Do your magic here
    }

    public function view()
    {
        $data['gates'] = $this->Gates->Get();
        $this->load->view('gates/view/index', $data, FALSE);
    }

    public function form($gate_id = null)
    {
        if($gate_id != null) {
            $data['action'] = 'update';
            $data['gate'] = $this->Gates->Get($gate_id);
        } else {
            $data['action'] = 'create';
        }
        $this->load->view('gates/form/index', $data, FALSE);
    }

    public function result($gate_id, $gate_from = null)
    {
        $data['timings'] = $this->Gates->GetAllTiming($gate_id);
        $data['gate'] = $this->Gates->Get($gate_id);
        $data['gates'] = $this->Gates->GetTypeStart();
        $data['players'] = $this->Gates->GetPlayers($gate_id);

        $data['gate_from'] = $gate_from;
        $data['gate_from_timestamp'] = $this->Gates->GetLastTiming($gate_from);

        $this->load->view('gates/table/index', $data, FALSE);
    }

    public function action_create()
    {
        $data = [
            'id' => Uuid::uuid1(),
            'type' => $this->input->post('type'),
            'name' => $this->input->post('name'),
            'description' => $this->input->post('description'),
            'created_at' => CURRENT_TIMESTAMP,
            'status' => 1
        ];
        $this->db->insert('gates', $data);
        redirect('gates');
    }

    public function action_delete($gate_id)
    {
        $this->db->delete('gates', array('id' => $gate_id));
        redirect('gates');
    }

    public function action_update()
    {
        $data = [
            'name' => $this->input->post('name'),
            'description' => $this->input->post('description')
        ];
        $this->db->where('id', $this->input->post('id'));
        $this->db->update('gates', $data);
        redirect('gates');
    }

    public function action_update_status($gate_id, $status)
    {
        $this->db->set('status', $status);
        $this->db->where('id', $gate_id);
        $this->db->update('gates');
        redirect('gates');
    }

    public function action_player_delete($player_id, $gate_id)
    {
        $where = [
            'player_id' => $player_id,
            'gate_id' => $gate_id
        ];
        $this->db->delete('gates_players', $where);
        redirect('player/' . $gate_id);
    }

}

/* End of file Gates.php */
/* Location: ./application/controllers/Gates.php */