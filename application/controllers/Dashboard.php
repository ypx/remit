<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends DH_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_Gates', 'Gates');
    }

    public function form()
    {
        $data['gates'] = $this->Gates->GetActive();
        $this->load->view('dashboard/form/index', $data, FALSE);
    }

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */